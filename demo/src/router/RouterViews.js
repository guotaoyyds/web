import React from 'react';
import {
    Switch,
    Redirect,
    Route
} from "react-router-dom";
function RouterViews({ router }) {
    const routers = router.filter(item => item.component);
    const redirect = router.filter(item => !item.component);
    return (
        <Switch>
            {
                routers.map((item, index) => <Route path={item.path} key={index} render={(props) => {
                    if (item.children) {
                        return <item.component router={item.children} {...props} />
                    }
                }} />)
            }
            {
                redirect.map((item, index) => <Redirect to={item.to} from={item.from} key={index} />)
            }
        </Switch>
    );
}

export default RouterViews;