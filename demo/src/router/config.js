import Home from "../views/home/Home";
import Index from "../views/home/Index";
const router = [
    {
        from: '/',
        to: '/home/index'
    },
    {
        path: '/home',
        component: Home,
        children: [
            {
                path: '/home/index',
                component: Index
            }
        ]
    }
]
export default router;