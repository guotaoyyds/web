import React from 'react';
import RouterViews from "./router/RouterViews";
import router from "./router/config"
function App() {
  return (
    <RouterViews router={router} />
  );
}

export default App;